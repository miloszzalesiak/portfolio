//Navigation

const navToggle = document.querySelector('.navToggle');
const navLinks = document.querySelectorAll('.navLink');


navToggle.addEventListener('click', () => {
    document.body.classList.toggle('navOpen');
});

navLinks.forEach(link => {
  link.addEventListener('click', () => {
    document.body.classList.remove('navOpen');
  })
})

//Back to top function

button = document.getElementById("btnTop");

window.onscroll = function () {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    button.style.display = "block";
  } else {
    button.style.display = "none";
  }
}

function backtopFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
